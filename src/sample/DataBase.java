package sample;

import java.sql.*;

public class DataBase {
    private Connection connection = null;
    private static DataBase baseConnection = null;

    private DataBase(String u, String p, String n) {
        String url = "jdbc:mysql://localhost:3306/";
        try {
            connection = DriverManager.getConnection(url, u, p);
            System.out.println("Active Connection!");
            try {
                Statement statement = connection.createStatement();
                int sqlQueryResult = statement.executeUpdate("CREATE DATABASE IF NOT EXISTS " + n);
                if (sqlQueryResult != 0) {
                    System.out.println("New Database Created!");
                } else {
                    System.out.println("Database Exist");
                }
                connection = DriverManager.getConnection(url + n, u, p);
            } catch (Exception ex) {
                System.out.println("Error in Statement " + ex.getClass());
            }
        } catch (SQLException sql) {
            System.out.println("Connection Failed!");
        }

    }

    public Connection connect() {
        return connection;
    }

    public static DataBase getConnection(String username, String password, String databaseName) {
        if (baseConnection == null) {
            baseConnection = new DataBase(username, password, databaseName);
        }
        return baseConnection;
    }


    public void createTablePlayer() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SHOW TABLES LIKE 'player'");
            int n = preparedStatement.executeUpdate();
            if (n == 0) {
                PreparedStatement createtable = connection.prepareStatement("CREATE TABLE player (\n" +
                        "\tid INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,\n" +
                        "\tfname VARCHAR(60) NOT NULL,\n" +
                        "\taction INT(2),\n" +
                        ")");
                createtable.executeUpdate();
                System.out.println("Table player create");
            } else {
                System.out.println("Table player Exist");
            }
        } catch (SQLException e) {
            System.out.println("Error In Create Table player ");
            e.printStackTrace();
        }
    }

    public void createTables() {
        createTablePlayer();
    }
}
